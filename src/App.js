import React, { Component } from 'react';
import './App.css';
import ImageCard from './ImageCard';

class App extends Component {
  state = {
      images: [],
      numberOfImages: 12,
      isScrolling: false
  }

  _getImages() {
    const { numberOfImages } = this.state;
      const imagesArr = [];
      for (let imageNumber = 0; imageNumber < numberOfImages; imageNumber++) {
          const cacheBust = imageNumber*(Math.ceil(Math.random(0, 10) * 10));
          imagesArr.push(`https://source.unsplash.com/random/1600x900?a=${cacheBust}`);
      }
      return imagesArr;
  }

  _getScrollListener() {}

  componentDidMount() {
    window.addEventListener('onscroll', this._getScrollListener);
    this.setState({
      images: this._getImages()
    });
  }

  render() {
    const { isScrolling } = this.state;
    return (
      <div className={`app-area ${isScrolling ? 'invert' : ''}`}>
        <div className="app-header">
          <h1 className="app-title">instarand</h1>
        </div>
        <div className="app-image-grid-flex">
          {this.state.images.map((imageSrc, i) => <ImageCard key={i} src={imageSrc} />)}
        </div>
      </div>
    );
  }
}

export default App;
