import React, { Component } from 'react';
import './ImageCard.css';

class ImageCard extends Component {
    render() {
        return (
            <div className="image-wrapper">
                <img class="image-unsplash" src={this.props.src} />
            </div>
        );
    }
}

export default ImageCard;